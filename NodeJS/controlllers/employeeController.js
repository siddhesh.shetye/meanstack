const express = require('express');
var router = express.Router();
var ObjectID = require('mongoose').Types.ObjectId;
var { Employee } = require('../models/employees');

//getAll
router.get('/', (req, res) => {
    Employee.find((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while retriving records : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

//get
router.get('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send('No record with given id : ' + req.params.id);

    Employee.findById(req.params.id, (err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while retriving a record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

//create
router.post('/', (req, res) => {
    var newRecord = new Employee({
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary,
    })

    newRecord.save((err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while creating new record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

//update
router.put('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send('No record with given id : ' + req.params.id);

    var updateRecord = {
        name: req.body.name,
        position: req.body.position,
        office: req.body.office,
        salary: req.body.salary,
    }

    Employee.findByIdAndUpdate(req.params.id, { $set: updateRecord }, { new: true }, (err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while updating a record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

//delete
router.delete('/:id', (req, res) => {
    if (!ObjectID.isValid(req.params.id))
        return res.status(400).send('No record with given id : ' + req.params.id);

    Employee.findByIdAndRemove(req.params.id, (err, docs) => {
        if (!err) {
            res.send(docs);
        } else {
            console.log('Error while deleting a record : ' + JSON.stringify(err, undefined, 2))
        }
    })
})

module.exports = router;