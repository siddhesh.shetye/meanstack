const express = require('express')
const bodyParser = require('body-parser')
const { mongoose } = require('./db')
const cors = require('cors')
var employeeController = require('./controlllers/employeeController');
var app = express()

app.use(bodyParser.json())
app.use(cors({ origin: 'http://localhost:4200' }))
app.listen(4000, () => console.log('Server started at : 4000'))

app.use('/employees', employeeController)